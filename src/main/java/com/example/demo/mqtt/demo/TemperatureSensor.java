package com.example.demo.mqtt.demo;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Created by t on 11/11/2018.
 */
public class TemperatureSensor implements Runnable {

    public static final String TOPIC = "fun";
    private final IMqttClient client;
    private CountDownLatch countDownLatch;

    public TemperatureSensor(IMqttClient client, CountDownLatch countDownLatch) {
        this.client = client;
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        if (client.isConnected()) {

            while (countDownLatch.getCount() > 0) {

                MqttMessage msg = readEngineTemp();
                msg.setQos(0);
                msg.setRetained(true);

                try {
                    client.publish(TOPIC, msg);
                } catch (MqttException e) {
                    e.printStackTrace();
                }

                try {
                    TimeUnit.SECONDS.sleep(2l);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                countDownLatch.countDown();
            }
        }
    }

    private MqttMessage readEngineTemp() {
        double temp = 80 + Math.random() * 20.0;
        byte[] payload = String.format("T:%04.2f", temp)
                .getBytes();
        return new MqttMessage(payload);
    }
}
