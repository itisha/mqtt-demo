package com.example.demo.mqtt.demo;

import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * Created by t on 11/11/2018.
 */
public class MqttDemo {

    public static void main(String[] args) throws MqttException, ExecutionException, InterruptedException {

        String publisherId = UUID.randomUUID().toString();
        IMqttClient publisher = new MqttClient("tcp://localhost:1883", publisherId);

        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);
        publisher.connect(options);

        String subscriberId = UUID.randomUUID().toString();
        IMqttClient subscriber = new MqttClient("tcp://localhost:1883", subscriberId);
        subscriber.connect(options);
        subscriber.subscribe(TemperatureSensor.TOPIC, (topic, msg) -> {
            byte[] payload = msg.getPayload();
            System.out.println(msg.toString());
        });

        CountDownLatch countDownLatch = new CountDownLatch(5);
        new Thread(new TemperatureSensor(publisher, countDownLatch)).start();
        countDownLatch.await();

        publisher.disconnect();
        subscriber.disconnect();
    }
}
