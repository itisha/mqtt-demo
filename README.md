This project uses **Spring Boot** parent `.pom` dependency.

But also it defines **MQTT Client** dependency explicitly.

Expecting running **MQTT** broker at `localhost:1883.`